import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) { }
    /* metodo buscar data paginada */
    getData(url: any): Observable<any> {
        return this.http.get<any>(url);
    }
}