import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { state,trigger,style,transition,animate } from '@angular/animations';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    animations: [
        trigger('changeState', [
            state ('state1', style({
                backgroundColor:'red'
            })),
            state ('state2', style({
                backgroundColor:'black'
            })),
            transition('state1<=>state2', [animate(2000)]),
        ])
    ]
})
export class ListComponent implements OnInit {
    public data:any;
    public response:any;
    public contador:number;
    public fixData:any;
    public next:any;
    public previous:any;
    public limit:any;
    public offset:any;
    /* api */
    private UrlApi:any;
    /* animaciones */
    public state:any;

    constructor(private dataApi: ApiService, private route: ActivatedRoute) { 
        this.contador = 0;
        /* limite de 50 y offset= 10 */
        this.fixData = [];
        this.next = "";
        this.previous = "";
        this.limit = 50;
        this.offset = 10;
        this.UrlApi = "";
        this.state = 'state1';
    }

    ngOnInit(): void {
        this.UrlApi = 'https://pokeapi.co/api/v2/pokemon/?limit='+this.limit+'&offset='+this.offset;
        this.getDataView(this.UrlApi);
    }

    getDataView(url:any) {        
        this.dataApi.getData(url).subscribe((res) => {
            this.response = res;
            this.contador = res.count;
            this.next = res.next;
            this.previous = res.previous;
            /* arma los datos para la vista */
            this.getItems(res.results);
        },
            (error) => { console.error(error); }
        );
    }
    /* animar */
    animacion(){
        this.state = this.state === 'state1' ? 'state2':'state1' ;
    }
    /* ordenar los datos para la vista */
    getItems(data:any){
        let count = 0;
        data.forEach((element: any) => {
            count++;
            this.getPokemonbyId(element.url,count)
        });
    }
    /* traer datos del pokemos */
    getPokemonbyId(url:any,contador:number){
        this.UrlApi = url;
        this.fixData = [];
        this.dataApi.getData(this.UrlApi).subscribe((res) => {
            this.fixData.push({
                count:contador,
                id:res.id,
                name:res.name,
                img:res.sprites.front_default,
                experience:res.base_experience
            });
            
           /* ordenamiento de datos */
            this.fixData.sort(function (a:any, b:any){
                return (a.count - b.count)
            })
        },
            (error) => { console.error(error); }
        );
    }
    getNextPaginate(){
        this.UrlApi = this.next;
        this.animacion();
        if(this.UrlApi != null){
            this.getDataView(this.UrlApi);
        }
    }
    getPrevioustPaginate(){
        this.UrlApi = this.previous;
        this.animacion();
        if(this.UrlApi != null){
            this.getDataView(this.UrlApi);
        }
    }
    /* cambio de selects */
    onChange(){
        this.UrlApi = 'https://pokeapi.co/api/v2/pokemon/?limit='+(this.limit-1)+'&offset='+(this.offset-1);
        this.getDataView(this.UrlApi);
    }
}
