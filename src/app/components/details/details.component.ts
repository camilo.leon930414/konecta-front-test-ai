import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { state,trigger,style,transition,animate } from '@angular/animations';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.css'],
    animations: [
        trigger('changeState', [
            state ('state1', style({
                color:'white'
            })),
            state ('state2', style({
                color:'red'
            })),
            state ('state3', style({
                color:'blue'
            })),
            state ('state4', style({
                color:'white'
            })),
            transition('state1 =>state2', [animate(3000)]),
            transition('state2 =>state3', [animate(3000)]),
            transition('state3 =>state4', [animate(3000)]),
            transition('state4 =>state1', [animate(3000)])
        ])
    ]
})
export class DetailsComponent implements OnInit {
    public fixData:any;
    /* animaciones */
    public state:any;
    /* api */
    private UrlApi:any;

    constructor(private ruta: ActivatedRoute,private dataApi: ApiService) {
        this.UrlApi = 'https://pokeapi.co/api/v2/pokemon/';
        this.fixData = [];
        this.state = 'state1';
    }

    ngOnInit(): void {
        this.getDetail(this.ruta.snapshot.params.id);
        this.animation();
    }
    /* anumacion */
    animation(){
        if(this.state === 'state1'){this.state = 'state2'}
        else if(this.state === 'state2'){this.state = 'state3'}
        else if(this.state === 'state3'){this.state = 'state4'}
        else if(this.state === 'state4'){this.state = 'state1'}
    }
    /* traer detalles */
    getDetail(id:number){
        this.fixData = [];
        this.dataApi.getData(this.UrlApi+id).subscribe((res) => {
            this.fixData = {
                id:res.id,
                name:res.name.toUpperCase(),
                sprites:this.getImages(res.sprites),
                experience:res.base_experience,
                ability : this.getAbilities(res.abilities),
                species:this.getSpecie(res.species),
                stats:this.getStats(res.stats),
                types:this.getTypes(res.types),
                weight:res.weight,
                height:res.height
            };
        },
            (error) => { console.error(error); }
        );
    }

    getImages(images:any){
        let images_ :any = [];
        Object.keys(images).forEach((index: any) => {
            if(images[index] != null && typeof images[index] =='string'){
                images_.push(images[index]);
            }
        });
        return images_;
    }
    /* logica para traer los demas datos */
    getAbilities(abilities:any){
        let data :any = [];
        abilities.forEach((item: any) => {
            this.dataApi.getData(item.ability.url).subscribe((res) => {
                data.push({
                    name:res.name,
                    generation:res.generation.name,
                });
            },
                (error) => { console.error(error); }
            );
        });
        return data;
    }
    /* traer estadisticas */
    getStats(stats:any){
        let data :any = [];
        stats.forEach((item: any) => {
            data.push({
                name:item.stat.name,
                base_stat:item.base_stat,
                effort:item.effort
            });
        });
        return data;
    }
    /* traer especie */
    getSpecie(specie:any){
        let data :any = [];
        this.dataApi.getData(specie.url).subscribe((res) => {
            data.push({
                name:res.name!= undefined ?res.name:'Sin datos',
                shape:res.shape!= undefined ?res.shape.name:'Sin datos',
                site:res.habitat!= undefined ?res.habitat.name:'Sin datos',
                evolves_from_species:res.evolves_from_species != undefined ?  res.evolves_from_species.name:'Sin datos',
                color:res.color!= undefined ?res.color.name:'Sin datos',
                base_happiness:res.base_happiness!= undefined ?res.base_happiness:'Sin datos',
                capture_rate:res.capture_rate!= undefined ?res.capture_rate:'Sin datos'
            });
        },
        (error) => { console.error(error); }
        );
        return data;
    }
    /* traer tipo */
    getTypes(types:any){
        let data :any = [];
        types.forEach((item: any) => {
            data.push({
                name:item.type.name,
                slot:item.slot,
            });
        });
        return data;
    }
}
